<?php
namespace BooklyGroupBooking\Frontend\Modules\ModernBookingForm\ProxyProviders;

use Bookly\Lib as BooklyLib;

class Shared extends \Bookly\Frontend\Modules\ModernBookingForm\Proxy\Shared
{
    /**
     * @inheritDoc
     */
    public static function prepareAppearance( array $bookly_options )
    {
        $bookly_options['show_nop'] = isset( $bookly_options['show_nop'] ) ? (bool) $bookly_options['show_nop'] : true;
        $bookly_options['l10n']['nop'] = __( 'Number of persons', 'bookly' );

        return $bookly_options;
    }
}